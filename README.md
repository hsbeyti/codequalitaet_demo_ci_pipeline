This Project is intended to serve as proof of concpet for the following Concept:
Codequalität Sicherung Konzeptmodell.
The follwing steps are implemented:
    Codequalität Messen und Bericht erstellen
    Codequalitäts-bericht analysieren basierend auf Kriterien
    Maßnahmen basierend auf Analyseergebnisse durchführen
    Analyseergebnisse publizieren
