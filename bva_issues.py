import gitlab
#import requests

# private token or personal token authentication (self-hosted GitLab instance)
gl = gitlab.Gitlab('https://git.zssi.ivbb.bund.de/')
# list all the projects
projects = gl.projects.list(visibility='public')
for project in projects:
    print(project.attributes)
    issues = project.issues.list(state='closed')
    for issue in issues:
        print(issue)
