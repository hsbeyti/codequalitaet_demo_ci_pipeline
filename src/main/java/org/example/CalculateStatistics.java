package org.example;

public class CalculateStatistics {
    public int getAverage(int[] list) {
        int average = 0;
        if (list.length == 0)
            return 0;
        for (int i = 0; i < list.length; i++) {
            average+= list[i];
        }
        return average / (list.length - 1);
    }

    public boolean createComplexCode(){
        boolean testOut=false;
        
        int x=1, y=4;
        
       if(y>x || y==3)
            return false;
       if(y<x || y==2)
            return true;
        if(x>y || x==3)
            return false;
        if(x>y || x==2)
            return true;
        if(y>x || y==3)
            return false;
        if(y<x || y==2)
            return true;
        if(x>y || x==3)
            return false;
        if(x>y || x==2)
            return true;
        if(y>x || y==3)
            return false;
        if(y<x || y==2)
            return true;
        else if((x<y || y==3) && testOut==false)
            return false;
        if((y<x || y==5) && testOut==false)
            return true;
        else if((x<3 || y==2) && testOut==false)
            return false;
        else if((x<y || y==2) && testOut==true)
            return false;
        if((x<2 || y==7) && testOut==false)
            return true;
        else if((x<y || y==9) && testOut==false)
            return false;
        return   testOut;
    }
}
